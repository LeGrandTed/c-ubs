#!/bin/bash

[ -e /tmp/minipng-samples ] && { echo "/tmp/minipng-samples already exists"; exit 1; }
mkdir -p /tmp/minipng-samples/{bw,other}/{,n}ok

cp A.mp black.mp split-black.mp uneven-dimensions.mp unordered_A.mp /tmp/minipng-samples/bw/ok
cp wrong-magic.mp broken-dimensions.mp broken-dimensions2.mp missing-data.mp missing-header.mp /tmp/minipng-samples/bw/nok
cp gray.mp damier.mp french-flag.mp french-palette.mp german-flag-palette.mp /tmp/minipng-samples/other/ok
cp german-flag-error-palette.mp /tmp/minipng-samples/other/nok

cd /tmp/
tar cvzf minipng-samples.tgz minipng-samples
rm -rf minipng-samples
cd -
mv -i /tmp/minipng-samples.tgz .
