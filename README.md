# Projet de C L3 SNIO Semestre 5 2022

## Description:

Un parser d’images au format MiniPNG, une version simplifiée du format d’images standard PNG

## Commandes Makefile:

  * <u>make</u> : créer un fichier bin et compile le projet

  * <u>make run</u> : execute le projet

  * <u>make clean</u> : vide et supprime le fichier bin