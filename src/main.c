#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define MaxBytes 100000
#define DescriptionSize 5

typedef struct
{
	int width;
	int height;
	int type;
}HeaderData;


typedef struct
{
	int start;
	int length;
}ComData;


typedef struct
{
	HeaderData headerData;
	int imgStart;
	int blockImgSize;
}ImgData;


typedef struct
{
	int index;
	char blockLetter;
	int blockSize;
	int dataSize;
}Block;


void displayCom(ComData data, int * buffer){
	int comLength = data.length;
	char com[comLength+1];
	for (int i = 0 ; i < comLength ; ++i)
	{
		com[i] = buffer[i+data.start];
	}
	com[comLength] = '\0';

	printf("Comments : %s\n", com);

}

void displayHeader(HeaderData data){
	char * colorType;

	switch(data.type){
		case 0:
			colorType = "0 (noir et blanc)";
			break;
		case 1:
			colorType = "1 (Nuances de gris)";
			break;
		case 2:
			colorType = "2 (palette)";
			break;
		case 3:
			colorType = "3 (RGB)";
			break;
		default:
			colorType= "Unknown";
			break;
	}
	printf("Largeur : %d\nHauteur : %d\nType : %s\n",data.width, data.height, colorType);

}

int getBlocks(Block * blocks,int * nbBlocks,int * buffer, int bufferSize){
	int blocksIndex=0;
	int letterIndex=0;
	char tmpLetter=0;
	int tmpSize = 0;
	int byteIndex=8;

	while(byteIndex<bufferSize-1){
		int tmpSize = 0;
		letterIndex = byteIndex;
		
		tmpLetter = buffer[byteIndex];
		++byteIndex;
		for (int i = 0; i < 4; ++i)
		{
			int power  = (int)pow(256,3-i);
			tmpSize = tmpSize+(buffer[byteIndex+i]*(power));
		}
		byteIndex = byteIndex + 4;
		if (nbBlocks == 0 && tmpLetter != 'H' )
		{
			return 1;
		}
		//printf("index:%d lettre :(hexa) %x /(char)%c, taille :(hexa) %x /(dec)%d \n",byteIndex,tmpLetter,tmpLetter,tmpSize,tmpSize);

		Block tmpBlock = {letterIndex,tmpLetter,tmpSize+DescriptionSize,tmpSize};
		blocks[blocksIndex]=tmpBlock;
		++blocksIndex;
		byteIndex = byteIndex+tmpSize;

	}

	*nbBlocks = blocksIndex;
	return 0;
}


ImgData getDataFromBlocks(Block * blocks,int nbBlocks,int * buffer){
	int width=0;
	int height=0;
	int type=0;
	int comStart = 0;
	int comLength=0;
	int imgStart=0;
	int blockImgSize=0;
	int dataStart;
	HeaderData headerData;

	for (int i = 0; i < nbBlocks; ++i)
	{
		switch (blocks[i].blockLetter){
			case 'H':
				dataStart = blocks[i].index+DescriptionSize;
				for (int j = 1; j <= 4; ++j)
				{
					int power  = (int)pow(256,3-j);
					width = width+(buffer[dataStart+j]*(power));
					height = height+(buffer[dataStart+j+4]*(power));
				}

				type = buffer[(dataStart+blocks[i].dataSize)-1];

				headerData.width = width;
				headerData.height = height;
				headerData.type = type;
				displayHeader(headerData);
				break;

			case 'C':
				comStart = blocks[i].index+DescriptionSize;
				comLength = blocks[i].dataSize;
				ComData comData = {comStart,comLength};
				displayCom(comData,buffer);
				break;

			case 'D':
				imgStart = blocks[i].index+blocks[i].blockSize-blocks[i].dataSize;
				blockImgSize = blocks[i].dataSize;
				break;
			
		}
	}

	ImgData data = {headerData,imgStart,blockImgSize};

	return data;

}

void displayImg(ImgData * data, int * buffer)
{
	int imgBufferSize = ((data -> headerData.height)*(data -> headerData.width));
	int forMaxValue;
	if (!data->headerData.type)
	{
		forMaxValue = imgBufferSize/8;
	}else{
		forMaxValue = imgBufferSize;
	}

	for (int imgBufferIndex = 0; imgBufferIndex < forMaxValue ; ++imgBufferIndex)
	{
		if (!data->headerData.type)
		{
			int j=128;
			for (int bit = 0; bit < 8; ++bit)
			{
				int bitValue;
				bitValue = (buffer[imgBufferIndex+data->imgStart] & j) != 0x00;
				if(!bitValue){printf("\x1b[47m  \x1b");}else{printf("\x1b[40m  \x1b");}
				j = j/2;
				if (((bit+(imgBufferIndex*8))%data->headerData.width)==(data->headerData.width)-1)
				{
					printf("\x1b[m\n\x1b");
				}
			}
		}else{
			int bitValue;
			bitValue = (buffer[imgBufferIndex+data->imgStart]) != 0x00;
			if(!bitValue){printf("\x1b[47m  \x1b");}else{printf("\x1b[40m  \x1b");}
			if ((imgBufferIndex%data->headerData.width)==(data->headerData.width)-1)
				{
				printf("\x1b[m\n\x1b");

				}
		}
		
	}
		printf("\x1b[m\x1b");
			
}

int containsBlock(Block *blocks,int nbBlocks,char blockLetter){
	int res = 0;
	for (int i = 0; i < nbBlocks; ++i)
	{
		if (blockLetter == 'H')
		{
			if (blocks[i].blockLetter == blockLetter)
			{
				res = res + 1;
			}
		}else{
			if (blocks[i].blockLetter == blockLetter)
			{
				res = 1;
			}	
		}
		
	}
	if (res > 1)
	{
		return 0;
	}
	return res;
}

int integrityTest(Block *blocks,int nbBlocks,int * buffer,int sizeFile){
	
	if (sizeFile<8)
	{
		return 1;
	}
	int totalBlockSize = 9;

	for (int i = 0; i < nbBlocks; ++i)
	{
		totalBlockSize = blocks[i].blockSize + totalBlockSize;
	}

	if (totalBlockSize != sizeFile)
	{
		return 1;
	}

	char magic[] = "Mini-PNG";

	for (int i = 0; i < 8; ++i)
	{ 
		if(buffer[i] != magic[i]){
			return 1;
		}
	}
	if (!containsBlock(blocks,nbBlocks,'H'))
	{
		return 1;
	}
	if (!containsBlock(blocks,nbBlocks,'D'))
	{
		return 1;
	}

	return 0;
}

int main()
{
	int bufferSize = 0;
	int c = 0;
	FILE * file;

	file = fopen("./Sujet/Echantillons/A.mp","rb");

	if (file == NULL){
		printf("Fichier non trouvé\n");
		return 0;
	}

	// long int size = ftell(file);	
	int * buffer = (int *) malloc (MaxBytes);

	while(c!=EOF){
		c = fgetc(file);
		buffer[bufferSize] = c;
		bufferSize++;
	}
	Block * blocks = (Block *) malloc(sizeof(Block)*10);
	int nbBlocks=0;
	if (getBlocks(blocks,&nbBlocks,buffer,bufferSize)){
		printf("Fichier illisible.\n");
		free(buffer);
		fclose(file);
		return 0;
	}
	if (!integrityTest(blocks,nbBlocks,buffer,bufferSize))
	{
		ImgData dataFromBlocks = getDataFromBlocks(blocks,nbBlocks,buffer);
		displayImg(&dataFromBlocks, buffer);
	}else{
		printf("Fichier illisible.\n");
	}
	
	free(buffer);

	fclose(file);
	return 0;
}