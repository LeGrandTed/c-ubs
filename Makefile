main:main.o
	@gcc -o bin/main bin/main.o -lm
	
main.o:src/main.c
	@mkdir -p bin
	@gcc -o bin/main.o -c src/main.c 

run:
	@./bin/main

clean:
	@rm ./bin/*
	@rmdir bin